<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index() {
        $evs=$this->chargeDonnees();
        $evs=array_slice($evs, 0, 6);
        return $this->render('default/index.html.twig', [
            'evs' => $evs,
        ]);
    }

    /**
     * @Route("/apropos", name="apropos")
     */
    public function apropos() {
        return $this->render('default/apropos.html.twig');
    }

    /**
     * @Route("/evenements", name="evenements")
     */
    public function evenements() {
        $evs=$this->chargeDonnees();
        return $this->render('default/evenements.html.twig', [
            'evs'=>$evs
        ]);
    }

    /**
     * @Route("/evenement/{nom}", name="evenement")
     */
    public function evenement($nom) {
        $ev=null;
        $evs=$this->chargeDonnees();
        $i=0;
        $trouve=false;
        while($i<count($evs) && !$trouve):
            if($evs[$i]->getNom()==$nom):
                $ev=$evs[$i];
                $trouve=true;
            endif;
            $i++;
        endwhile;
        /* foreach moins performant...
        foreach ($evs as $evevement) :
            if($evevement->getNom()==$nom) :
                $ev=$evevement;
            endif;
        endforeach;
        */
        return $this->render('default/evenement.html.twig', [
            'ev'=>$ev
        ]);
    }

    /**
    * @Route("/panier/{nom}", name="panier")
    */
    public function panier($nom="", SessionInterface $session) {
        if ($nom != "") :
            $evs=$this->chargeDonnees();
            foreach ($evs as $evevement) :
                if ($evevement->getNom() == $nom) :
                    $panier=$session->get('panier',[]);
                    if (!in_array($nom, $panier)) :
                        $panier[] = $evevement->getNom();
                    endif;
                    $session->set('panier', $panier);
                endif;
            endforeach;
            return $this->redirectToRoute('panier');
        else :
            $panier=$session->get('panier',[]);
            return $this->render('default/panier.html.twig', [
                'panier'=> $panier
            ]);
        endif;
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request) {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) :
            $rep=$form->getData();
            // session temporaire très pratique
            // https://symfony.com/doc/current/controller.html#flash-messages
            $this->addFlash('notice','Merci '.$rep['nom'].'. Votre message a bien été envoyé !');
            // envoi du mail avec les données de $rep
            // en utilisant mailer : https://symfony.com/doc/current/mailer.html
            // ...
            return $this->redirectToRoute('contact');
        endif;
        return $this->render('default/contact.html.twig', ['form' => $form->createView()]);
    }

    private function chargeDonnees() {
        $ev1=new Evenement("test1",new \DateTime('2019-08-23'),"img/dummies/430x500a.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev2=new Evenement("test2",new \DateTime('2019-09-25'),"img/dummies/430x500b.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev3=new Evenement("test3",new \DateTime('2019-10-01'),"img/dummies/430x500c.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev4=new Evenement("test4",new \DateTime('2019-10-23'),"img/dummies/430x500d.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev5=new Evenement("test5",new \DateTime('2019-11-12'),"img/dummies/430x500e.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev6=new Evenement("test6",new \DateTime('2019-11-15'),"img/dummies/430x500f.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev7=new Evenement("test7",new \DateTime('2019-12-23'),"img/dummies/430x500a.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev8=new Evenement("test8",new \DateTime('2019-12-25'),"img/dummies/430x500b.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $ev9=new Evenement("test9",new \DateTime('2020-01-05'),"img/dummies/430x500c.jpg","Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aliquid beatae consectetur consequatur corporis, culpa delectus deserunt dolor dolorem dolorum ea exercitationem fuga id illum, incidunt iste iure, laborum libero modi natus nobis nulla officia officiis perferendis provident quam quasi quisquam quo quos repellendus saepe sunt temporibus totam unde veritatis vitae voluptas. Accusamus dignissimos, distinctio dolore enim eos esse est excepturi explicabo inventore libero pariatur perspiciatis quas quasi quod, sit? Accusamus amet assumenda atque distinctio eius expedita explicabo itaque labore magnam, magni molestias nobis non, pariatur perferendis quaerat qui sit veniam vero voluptatem voluptatum. Aliquid commodi debitis excepturi natus quas?");
        $evs=[$ev1,$ev2,$ev3,$ev4,$ev5,$ev6,$ev7,$ev8,$ev9];
        return $evs;
    }

}
